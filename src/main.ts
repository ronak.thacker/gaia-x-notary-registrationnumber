import { ConfigService } from '@nestjs/config'
import { NestFactory } from '@nestjs/core'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'

import * as packageJson from '../package.json'
import { AppModule } from './app.module'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  const configService = app.get(ConfigService)

  const globalPrefix = configService.get<string>('APP_PATH')?.trim() ?? ''
  app.setGlobalPrefix(globalPrefix)
  app.enableCors()

  const config = new DocumentBuilder()
    .addServer(globalPrefix)
    .setTitle('Notary')
    .setDescription('Gaia-X Notary module implementation to validate legal registration numbers')
    .setVersion(packageJson.version)
    .addTag('gx-notary')
    .build()
  const document = SwaggerModule.createDocument(app, config, {
    ignoreGlobalPrefix: true
  })
  SwaggerModule.setup(`${globalPrefix}/docs`, app, document)

  await app.listen(3000)
}
bootstrap()
