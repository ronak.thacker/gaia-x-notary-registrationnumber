import { DidUtils } from './did.utils'

describe('DidUtils', () => {
  it.each([
    ['http://localhost:3000/main', 'did:web:localhost:3000:main'],
    ['https://www.gaia-x.eu/', 'did:web:www.gaia-x.eu'],
    ['https://www.gaia-x.eu/main//', 'did:web:www.gaia-x.eu:main'],
    ['https://test.gaia-x.eu:1234', 'did:web:test.gaia-x.eu:1234']
  ])('should build a valid Did Web from base URL', (baseUrl, didWeb) => {
    expect(DidUtils.buildDidWebFromBaseUrl(baseUrl)).toEqual(didWeb)
  })

  it.each(['totally wrong', 'www.gaia-x.eu', 'www.gaia-x.eu/main', 'https://'])(
    'should throw an error when the base URL format is not compliant',
    baseUrl => {
      expect(() => DidUtils.buildDidWebFromBaseUrl(baseUrl)).toThrow(
        `${baseUrl} doesn't match the required base URL format (ie. https://gaia-x.eu:1234/main)`
      )
    }
  )
})
