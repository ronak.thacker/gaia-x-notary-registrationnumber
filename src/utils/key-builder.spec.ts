import { KeyBuilder } from './key-builder'

describe('KeyBuilder', () => {
  it('should provide a key pair', async () => {
    const { privateKey, publicKey } = KeyBuilder.buildKeyPair()

    const secret = 'This is a secret'
    const encryptedSecret = publicKey.encrypt(secret)

    expect(privateKey.decrypt(encryptedSecret)).toEqual(secret)
  })

  it.each(['1.2.840.113549.2.9', '1.2.840.113549.2.10'])('should provide correct JWS from OID', (oid: string) => {
    const pkAlgMapping = new Map<string, string>([
      ['1.2.840.113549.2.9', 'HS256'],
      ['1.2.840.113549.2.10', 'HS384']
    ])
    const privateKeyAlg = KeyBuilder.getPrivateKeyAlgorithm(oid)

    expect(privateKeyAlg).toEqual(pkAlgMapping.get(oid))
  })
})
