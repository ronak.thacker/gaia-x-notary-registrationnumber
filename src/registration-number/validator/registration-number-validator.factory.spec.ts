import { RegistrationNumberTypeEnum } from '../enum/registration-number-type.enum'
import { ValidationPayload } from '../model/validation/validation-payload'
import { EoriValidator } from './eori.validator'
import { EuidValidator } from './euid.validator'
import { LeiCodeValidator } from './lei-code.validator'
import { OpenCorporatesValidator } from './open-corporates.validator'
import { RegistrationNumberValidatorFactory } from './registration-number-validator.factory'
import { RegistrationNumberValidator } from './registration-number.validator'
import { VatIdValidator } from './vat-id.validator'

jest.mock('./eori.validator')
jest.mock('./lei-code.validator')
jest.mock('./euid.validator')
jest.mock('./vat-id.validator')

describe('RegistrationNumberValidatorFactory', () => {
  const eoriValidator: EoriValidator = Object.create(EoriValidator.prototype)
  const euidValidator: EuidValidator = Object.create(EuidValidator.prototype)
  const vatIdValidator: VatIdValidator = Object.create(VatIdValidator.prototype)
  const leiCodeValidator: LeiCodeValidator = Object.create(LeiCodeValidator.prototype)
  const openCorporatesValidator: OpenCorporatesValidator = Object.create(OpenCorporatesValidator.prototype)
  let factory: RegistrationNumberValidatorFactory

  beforeEach(async () => {
    factory = new RegistrationNumberValidatorFactory(eoriValidator, euidValidator, vatIdValidator, leiCodeValidator, openCorporatesValidator)
  })

  it.each([
    [RegistrationNumberTypeEnum.EORI, eoriValidator],
    [RegistrationNumberTypeEnum.EUID, euidValidator],
    [RegistrationNumberTypeEnum.VAT_ID, vatIdValidator],
    [RegistrationNumberTypeEnum.LEI_CODE, leiCodeValidator],
    [RegistrationNumberTypeEnum.TAX_ID, openCorporatesValidator]
  ])('should provide the right validator', (type: RegistrationNumberTypeEnum, validator: RegistrationNumberValidator<ValidationPayload>) => {
    expect(factory.validatorFor(type)).toStrictEqual(validator)
  })
})
