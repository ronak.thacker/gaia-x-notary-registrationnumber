import { Injectable } from '@nestjs/common'
import { Observable } from 'rxjs'

import { ValidationPayload } from '../model/validation/validation-payload'
import { RegistrationNumberValidator } from './registration-number.validator'

@Injectable()
export class EuidValidator implements RegistrationNumberValidator<ValidationPayload> {
  validate(euid: string): Observable<ValidationPayload> {
    // TODO: implement EUID check
    // Is there an unified API? Otherwise we may have to implement it per country.
    // https://ec.europa.eu/transparency/documents-register/api/files/SWD(2023)79?ersIds=de00000001046891 (2.3)
    throw new Error('EUID is not supported yet')
  }
}
