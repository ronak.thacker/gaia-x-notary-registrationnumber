import { EuidValidator } from './euid.validator'

describe('EuidValidator', () => {
  it('should throw error because not implemented', () => {
    const validator: EuidValidator = new EuidValidator()

    expect(() => validator.validate('test')).toThrow('EUID is not supported yet')
  })
})
