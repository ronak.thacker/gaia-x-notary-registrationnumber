import { HttpService } from '@nestjs/axios'
import { Injectable, Logger } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { DateTime } from 'luxon'
import { Observable, catchError, combineAll, map, of } from 'rxjs'

import { RegistrationNumberTypeEnum } from '../enum/registration-number-type.enum'
import { OpenCorporatesNotConfiguredException } from '../exceptions/open-corporates-not-configured.exception'
import { OpenCorporatesValidationPayload } from '../model/validation/open-corporates-validation-payload'
import { ValidationEvidence } from '../model/validation/validation-evidence'
import { RegistrationNumberValidator } from './registration-number.validator'

interface OpenCorporatesEntry {
  company_number: string
  opencorporates_url: string
  jurisdiction_code: string
}

@Injectable()
export class OpenCorporatesValidator implements RegistrationNumberValidator<OpenCorporatesValidationPayload> {
  private readonly logger = new Logger(OpenCorporatesValidator.name)

  private readonly openCorporatesApiUrl: string
  private readonly openCorporatesApiKey: string

  constructor(
    private readonly configService: ConfigService,
    private readonly httpService: HttpService
  ) {
    this.openCorporatesApiUrl = this.configService.get<string>('OPEN_CORPORATES_VALIDATION_API', '')?.trim()
    this.openCorporatesApiKey = this.configService.get<string>('OPEN_CORPORATES_VALIDATION_API_KEY', '')?.trim()

    if (!this.openCorporatesApiUrl) {
      throw new Error('Please provide the Open Corporates validation service URL through OPEN_CORPORATES_VALIDATION_API')
    }

    // Validation of API KEY purposefully moved to the validate method
    // Since the OpenCorporates validator is optional, it should only fail on a request, not on initialization

    this.logger.debug(`Initializing Open Corporates local registration number validator with (API: ${this.openCorporatesApiUrl})`)
  }

  // See: https://api.opencorporates.com/documentation/API-Reference
  validate(registrationNumber: string): Observable<OpenCorporatesValidationPayload> {
    if (!registrationNumber || !registrationNumber.trim()) {
      throw new Error('The local registration number was not provided')
    }

    if (!this.openCorporatesApiKey) {
      this.logger.error(
        'Access to OpenCorporates not configured. Please provide the Open Corporates API key through OPEN_CORPORATES_VALIDATION_API_KEY'
      )
      throw new OpenCorporatesNotConfiguredException()
    }

    this.logger.log(`Validating local registration number ${registrationNumber} against the Open Corporates API`)

    const queryParams = {
      q: registrationNumber,
      fields: 'company_number',
      inactive: false,
      api_token: this.openCorporatesApiKey
    }

    return this.httpService
      .get(this.openCorporatesApiUrl, {
        params: queryParams
      })
      .pipe(
        map(response => {
          const companies: OpenCorporatesEntry[] = response.data.results.companies.map(c => c.company)

          if (companies.length == 0) {
            return this.validationFailurePayload(registrationNumber)
          }

          // TODO: What to do when we have more companies?
          const match = companies.at(0)
          const countryCode = match.jurisdiction_code.toUpperCase()

          return new OpenCorporatesValidationPayload(
            RegistrationNumberTypeEnum.TAX_ID,
            match.company_number,
            true,
            countryCode,
            new ValidationEvidence(match.opencorporates_url, DateTime.now(), RegistrationNumberTypeEnum.TAX_ID)
          )
        }),
        catchError(error => {
          this.logger.warn(`Failed to validate local registration number with value ${registrationNumber} : ${error}`)

          return of(this.validationFailurePayload(registrationNumber))
        })
      )
  }

  private validationFailurePayload(registrationNumber: string): OpenCorporatesValidationPayload {
    return new OpenCorporatesValidationPayload(
      RegistrationNumberTypeEnum.TAX_ID,
      registrationNumber,
      false,
      null,
      new ValidationEvidence(this.openCorporatesApiUrl, DateTime.now(), RegistrationNumberTypeEnum.TAX_ID)
    )
  }
}
