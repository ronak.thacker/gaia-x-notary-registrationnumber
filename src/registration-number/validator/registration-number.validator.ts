import { Observable } from 'rxjs'

import { ValidationPayload } from '../model/validation/validation-payload'

export interface RegistrationNumberValidator<T extends ValidationPayload> {
  validate(registrationNumber: string): Observable<T>
}
