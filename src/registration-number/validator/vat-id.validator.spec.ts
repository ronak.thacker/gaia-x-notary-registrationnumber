import { HttpModule, HttpService } from '@nestjs/axios'
import { Logger } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { Test } from '@nestjs/testing'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import { DateTime } from 'luxon'

import { ConfigServiceMock } from '../../common/test/config-service.mock'
import { RegistrationNumberTypeEnum } from '../enum/registration-number-type.enum'
import { VatIdValidationPayload } from '../model/validation/vat-id-validation-payload'
import { RegistrationNumberTestUtils } from '../test/registration-number-test.utils'
import { VatIdValidator } from './vat-id.validator'

describe('VatIdValidator constructor', () => {
  it.each(['', undefined, null, ' '])('should throw an error when VAT_ID_VALIDATION_API is missing', vatIdValidationApi => {
    const configService: ConfigService = new ConfigService()
    const httpService: HttpService = new HttpService()

    jest.spyOn(configService, 'get').mockImplementation(() => vatIdValidationApi)

    expect(() => new VatIdValidator(configService, httpService)).toThrow(
      'Please provide the VAT ID validation service URL through VAT_ID_VALIDATION_API'
    )
  })
})

describe('VatIdValidator', () => {
  const checkVatService = 'https://validator.eu/checkVatService'

  let axiosMock: MockAdapter
  let vatIdValidator: VatIdValidator

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [ConfigModule, HttpModule],
      providers: [VatIdValidator]
    })
      .overrideProvider(ConfigService)
      .useValue(
        new ConfigServiceMock({
          VAT_ID_VALIDATION_API: checkVatService
        })
      )
      .setLogger(new Logger())
      .compile()

    vatIdValidator = moduleRef.get<VatIdValidator>(VatIdValidator)

    axiosMock = new MockAdapter(axios)
  })

  it('should validate the VAT ID against the Check VAT API', done => {
    axiosMock.onPost(checkVatService).reply(
      200,
      `<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
          <env:Header/>
          <env:Body>
              <ns2:checkVatResponse xmlns:ns2="urn:ec.europa.eu:taxud:vies:services:checkVat:types">
                  <ns2:countryCode>FR</ns2:countryCode>
                  <ns2:vatNumber>87880851399</ns2:vatNumber>
                  <ns2:requestDate>${DateTime.now().toISODate()}</ns2:requestDate>
                  <ns2:valid>true</ns2:valid>
                  <ns2:name>M KELLEHER VINCENT</ns2:name>
                  <ns2:address>33 ALL JEAN RIBEREAU-GAYON
      33600 PESSAC</ns2:address>
              </ns2:checkVatResponse>
          </env:Body>
      </env:Envelope>`
    )

    vatIdValidator.validate(RegistrationNumberTestUtils.VAT_ID).subscribe({
      next(result: VatIdValidationPayload) {
        expect(result.type).toEqual(RegistrationNumberTypeEnum.VAT_ID)
        expect(result.valid).toEqual(true)
        expect(result.value).toEqual(RegistrationNumberTestUtils.VAT_ID)
        expect(result.countryCode).toEqual('FR')
        expect(result.evidence.url).toEqual(checkVatService)
        expect(result.evidence.executionDate.diffNow().milliseconds).toBeLessThan(5000)
        expect(result.evidence.of).toEqual(RegistrationNumberTypeEnum.VAT_ID)
        done()
      },
      error(err) {
        done(err)
      }
    })
  }, 2000)

  it('should manage invalid VAT IDs', done => {
    axiosMock.onPost(checkVatService).reply(
      200,
      `<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
          <env:Header/>
          <env:Body>
              <ns2:checkVatResponse xmlns:ns2="urn:ec.europa.eu:taxud:vies:services:checkVat:types">
                  <ns2:countryCode>FR</ns2:countryCode>
                  <ns2:vatNumber>8788085138</ns2:vatNumber>
                  <ns2:requestDate>${DateTime.now().toISODate()}</ns2:requestDate>
                  <ns2:valid>false</ns2:valid>
                  <ns2:name>---</ns2:name>
                  <ns2:address>---</ns2:address>
              </ns2:checkVatResponse>
          </env:Body>
      </env:Envelope>`
    )

    const invalidVatId = 'FR8788085138'
    vatIdValidator.validate(invalidVatId).subscribe({
      next(result: VatIdValidationPayload) {
        expect(result.type).toEqual(RegistrationNumberTypeEnum.VAT_ID)
        expect(result.valid).toEqual(false)
        expect(result.value).toEqual(invalidVatId)
        expect(result.countryCode).toEqual('FR')
        expect(result.evidence.url).toEqual(checkVatService)
        expect(result.evidence.executionDate.diffNow().milliseconds).toBeLessThan(5000)
        expect(result.evidence.of).toEqual(RegistrationNumberTypeEnum.VAT_ID)
        done()
      },
      error(err) {
        done(err)
      }
    })
  }, 2000)

  it.each(['', ' ', undefined, null])('should manage empty VAT IDs', emptyVatId => {
    expect(() => vatIdValidator.validate(emptyVatId)).toThrow('The VAT ID was not provided')
  })

  it('should manage malformed VAT IDs', done => {
    axiosMock.onPost(checkVatService).reply(
      200,
      `<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
          <env:Header/>
          <env:Body>
              <env:Fault>
                  <faultcode>env:Server</faultcode>
                  <faultstring>INVALID_INPUT</faultstring>
              </env:Fault>
          </env:Body>
      </env:Envelope>`
    )

    const malformedVatId = 'F878808513'
    vatIdValidator.validate(malformedVatId).subscribe({
      next(result: VatIdValidationPayload) {
        assertErrorResponse(result, malformedVatId)
        done()
      },
      error(err) {
        done(err)
      }
    })
  }, 2000)

  it('should return a proper message on rate-limiting', done => {
    axiosMock.onPost(checkVatService).reply(
      200,
      `<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
          <env:Header/>
          <env:Body>
              <env:Fault>
                <faultcode>env:Server</faultcode>
                <faultstring>MS_MAX_CONCURRENT_REQ</faultstring>
              </env:Fault>
          </env:Body>
      </env:Envelope>`
    )

    const validVAT = 'FR87880851399'
    vatIdValidator.validate(validVAT).subscribe({
      next(result: VatIdValidationPayload) {
        expect(result.invalidationReason).toEqual(`The VAT ID ${validVAT} could not be validated due to rate limiting in the VAT API`)
        done()
      },
      error() {}
    })
  }, 2000)

  it('should manage server errors', done => {
    axiosMock.onPost(checkVatService).reply(500)

    vatIdValidator.validate(RegistrationNumberTestUtils.VAT_ID).subscribe({
      next(result: VatIdValidationPayload) {
        assertErrorResponse(result, RegistrationNumberTestUtils.VAT_ID)
        done()
      },
      error(err) {
        done(err)
      }
    })
  }, 2000)

  function assertErrorResponse(result: VatIdValidationPayload, invalidVatId: string) {
    expect(result.type).toEqual(RegistrationNumberTypeEnum.VAT_ID)
    expect(result.valid).toEqual(false)
    expect(result.value).toEqual(invalidVatId)
    expect(result.countryCode).toBeNull()
    expect(result.evidence.url).toEqual(checkVatService)
    expect(result.evidence.executionDate.diffNow().milliseconds).toBeLessThan(5000)
    expect(result.evidence.of).toEqual(RegistrationNumberTypeEnum.VAT_ID)
    expect(result.invalidationReason).toBeDefined()
  }
})
