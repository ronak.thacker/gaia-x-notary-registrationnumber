import { Injectable } from '@nestjs/common'

import { RegistrationNumberTypeEnum } from '../enum/registration-number-type.enum'
import { ValidationPayload } from '../model/validation/validation-payload'
import { EoriValidator } from './eori.validator'
import { EuidValidator } from './euid.validator'
import { LeiCodeValidator } from './lei-code.validator'
import { OpenCorporatesValidator } from './open-corporates.validator'
import { RegistrationNumberValidator } from './registration-number.validator'
import { VatIdValidator } from './vat-id.validator'

@Injectable()
export class RegistrationNumberValidatorFactory {
  private readonly validators: { [type in RegistrationNumberTypeEnum]: RegistrationNumberValidator<ValidationPayload> }

  constructor(
    private readonly eoriValidator: EoriValidator,
    private readonly euidValidator: EuidValidator,
    private readonly vatIdValidator: VatIdValidator,
    private readonly leiCodeValidator: LeiCodeValidator,
    private readonly openCorporatesValidator: OpenCorporatesValidator
  ) {
    this.validators = {
      [RegistrationNumberTypeEnum.EORI]: eoriValidator,
      [RegistrationNumberTypeEnum.EUID]: euidValidator,
      [RegistrationNumberTypeEnum.VAT_ID]: vatIdValidator,
      [RegistrationNumberTypeEnum.LEI_CODE]: leiCodeValidator,
      [RegistrationNumberTypeEnum.TAX_ID]: openCorporatesValidator
    }
  }

  validatorFor(registrationNumberType: RegistrationNumberTypeEnum): RegistrationNumberValidator<ValidationPayload> {
    return this.validators[registrationNumberType]
  }
}
