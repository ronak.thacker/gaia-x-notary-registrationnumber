import { HttpModule, HttpService } from '@nestjs/axios'
import { Logger } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { Test } from '@nestjs/testing'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import { ConfigServiceMock } from '../../common/test/config-service.mock'
import { RegistrationNumberTypeEnum } from '../enum/registration-number-type.enum'
import { LeiCodeValidationPayload } from '../model/validation/lei-code-validation-payload'
import { RegistrationNumberTestUtils } from '../test/registration-number-test.utils'
import { LeiCodeValidator } from './lei-code.validator'

describe('LeiCodeValidator constructor', () => {
  it.each(['', undefined, null, ' '])('should throw an error when VAT_ID_VALIDATION_API is missing', leiCodeValidationApi => {
    const configService: ConfigService = new ConfigService()
    const httpService: HttpService = new HttpService()

    jest.spyOn(configService, 'get').mockImplementation(() => leiCodeValidationApi)

    expect(() => new LeiCodeValidator(configService, httpService)).toThrow('Please provide the GLEIF API URL through LEI_CODE_VALIDATION_API')
  })
})

describe('LeiCodeValidator', () => {
  const leiCodeValidationApi = 'https://gleif-validator.eu/'

  let axiosMock: MockAdapter
  let leiCodeValidator: LeiCodeValidator

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [ConfigModule, HttpModule],
      providers: [LeiCodeValidator]
    })
      .overrideProvider(ConfigService)
      .useValue(
        new ConfigServiceMock({
          LEI_CODE_VALIDATION_API: leiCodeValidationApi
        })
      )
      .setLogger(new Logger())
      .compile()

    leiCodeValidator = moduleRef.get<LeiCodeValidator>(LeiCodeValidator)

    axiosMock = new MockAdapter(axios)
  })

  it('should validate the LEI code agains the GLEIF API', done => {
    axiosMock.onGet(leiCodeValidationApi + RegistrationNumberTestUtils.LEI_CODE).reply(
      200,
      `{
  "data": {
    "type": "lei-records",
    "id": "529900W18LQJJN6SJ336",
    "attributes": {
      "lei": "529900W18LQJJN6SJ336",
      "entity": {
        "legalName": {
          "name": "Société Générale Effekten GmbH",
          "language": null
        },
        "otherNames": [],
        "transliteratedOtherNames": [],
        "legalAddress": {
          "language": null,
          "addressLines": [
            "Neue Mainzer Straße 46-50"
          ],
          "addressNumber": null,
          "addressNumberWithinBuilding": null,
          "mailRouting": null,
          "city": "Frankfurt am Main",
          "region": "DE-HE",
          "country": "DE",
          "postalCode": "60311"
        },
        "headquartersAddress": {
          "language": null,
          "addressLines": [
            "Neue Mainzer Straße 46-50"
          ],
          "addressNumber": null,
          "addressNumberWithinBuilding": null,
          "mailRouting": null,
          "city": "Frankfurt am Main",
          "region": "DE-HE",
          "country": "DE",
          "postalCode": "60311"
        },
        "registeredAt": {
          "id": "RA000242",
          "other": null
        },
        "registeredAs": "HRB 32283",
        "jurisdiction": "DE",
        "category": null,
        "legalForm": {
          "id": "2HBR",
          "other": null
        },
        "associatedEntity": {
          "lei": null,
          "name": null
        },
        "status": "ACTIVE",
        "expiration": {
          "date": null,
          "reason": null
        },
        "successorEntity": {
          "lei": null,
          "name": null
        },
        "otherAddresses": []
      },
      "registration": {
        "initialRegistrationDate": "2014-01-27T07:37:54Z",
        "lastUpdateDate": "2019-09-11T06:35:58Z",
        "status": "ISSUED",
        "nextRenewalDate": "2020-10-17T22:00:00Z",
        "managingLou": "5299000J2N45DDNE4Y28",
        "corroborationLevel": "FULLY_CORROBORATED",
        "validatedAt": {
          "id": "RA000242",
          "other": null
        },
        "validatedAs": "HRB 32283",
        "otherValidationAuthorities": [
          {
            "validatedAt": {
              "id": "RA000535"
            },
            "validatedAs": "B81335473"
          }
        ]
      },
      "bic": [
        "SGEFDEFFXXX",
        "SGEGDEF1XXX"
      ]
    },
    "relationships": {
      "managing-lou": {
        "links": {
          "related": "https://api.gleif.org/api/v1/lei-records/529900W18LQJJN6SJ336/managing-lou"
        }
      },
      "lei-issuer": {
        "links": {
          "related": "https://api.gleif.org/api/v1/lei-records/529900W18LQJJN6SJ336/lei-issuer"
        }
      },
      "direct-parent": {
        "links": {
          "relationship-record": "https://api.gleif.org/api/v1/lei-records/529900W18LQJJN6SJ336/direct-parent-relationship",
          "lei-record": "https://api.gleif.org/api/v1/lei-records/529900W18LQJJN6SJ336/direct-parent"
        }
      },
      "ultimate-parent": {
        "links": {
          "relationship-record": "https://api.gleif.org/api/v1/lei-records/529900W18LQJJN6SJ336/ultimate-parent-relationship",
          "lei-record": "https://api.gleif.org/api/v1/lei-records/529900W18LQJJN6SJ336/ultimate-parent"
        }
      },
      "direct-children": {
        "links": {
          "relationship-records": "https://api.gleif.org/api/v1/lei-records/529900W18LQJJN6SJ336/direct-child-relationships",
          "related": "https://api.gleif.org/api/v1/lei-records/529900W18LQJJN6SJ336/direct-children"
        }
      },
      "isins": {
        "links": {
          "related": "https://api.gleif.org/api/v1/lei-records/529900W18LQJJN6SJ336/isins"
        }
      }
    },
    "links": {
      "self": "https://api.gleif.org/api/v1/lei-records/529900W18LQJJN6SJ336"
    }
  }
}`
    )

    leiCodeValidator.validate(RegistrationNumberTestUtils.LEI_CODE).subscribe({
      next(result: LeiCodeValidationPayload) {
        expect(result.type).toEqual(RegistrationNumberTypeEnum.LEI_CODE)
        expect(result.valid).toEqual(true)
        expect(result.value).toEqual(RegistrationNumberTestUtils.LEI_CODE)
        expect(result.countryCodes['iso3166-1']).toEqual('DE')
        expect(result.countryCodes['iso3166-2']).toEqual('DE-HE')
        expect(result.evidence.url).toEqual(leiCodeValidationApi + RegistrationNumberTestUtils.LEI_CODE)
        expect(result.evidence.executionDate.diffNow().milliseconds).toBeLessThan(5000)
        expect(result.evidence.of).toEqual(RegistrationNumberTypeEnum.LEI_CODE)

        done()
      },
      error(err) {
        done(err)
      }
    })
  }, 2000)

  it('should manage the case where the LEI code does not exist', done => {
    axiosMock.onGet(leiCodeValidationApi + RegistrationNumberTestUtils.LEI_CODE).reply(404)

    leiCodeValidator.validate(RegistrationNumberTestUtils.LEI_CODE).subscribe({
      next(result: LeiCodeValidationPayload) {
        assertFailed(result)
        done()
      },
      error(err) {
        done(err)
      }
    })
  }, 2000)

  it('should manage server errors', done => {
    axiosMock.onGet(leiCodeValidationApi + RegistrationNumberTestUtils.LEI_CODE).reply(500)

    leiCodeValidator.validate(RegistrationNumberTestUtils.LEI_CODE).subscribe({
      next(result: LeiCodeValidationPayload) {
        assertFailed(result)
        done()
      },
      error(err) {
        done(err)
      }
    })
  }, 2000)

  function assertFailed(result: LeiCodeValidationPayload) {
    expect(result.type).toEqual(RegistrationNumberTypeEnum.LEI_CODE)
    expect(result.valid).toEqual(false)
    expect(result.value).toEqual(RegistrationNumberTestUtils.LEI_CODE)
    expect(result.countryCodes).toBeNull()
    expect(result.evidence.url).toEqual(leiCodeValidationApi + RegistrationNumberTestUtils.LEI_CODE)
    expect(result.evidence.executionDate.diffNow().milliseconds).toBeLessThan(5000)
    expect(result.evidence.of).toEqual(RegistrationNumberTypeEnum.LEI_CODE)
  }
})
