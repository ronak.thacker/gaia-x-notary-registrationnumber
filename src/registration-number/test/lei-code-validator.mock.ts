import { DateTime } from 'luxon'
import { Observable } from 'rxjs'

import { RegistrationNumberTypeEnum } from '../enum/registration-number-type.enum'
import { LeiCodeValidationPayload } from '../model/validation/lei-code-validation-payload'
import { ValidationEvidence } from '../model/validation/validation-evidence'
import { RegistrationNumberTestUtils } from './registration-number-test.utils'

export const LeiCodeValidatorMock = {
  validate: jest.fn((leiCode: string): Observable<LeiCodeValidationPayload> => {
    return new Observable(subscriber => {
      const valid = leiCode === RegistrationNumberTestUtils.LEI_CODE

      subscriber.next(
        new LeiCodeValidationPayload(
          RegistrationNumberTypeEnum.LEI_CODE,
          leiCode,
          valid,
          valid
            ? {
                'iso3166-1': 'FR',
                'iso3166-2': 'FR-NAQ'
              }
            : null,
          new ValidationEvidence('https://validator.eu/checkLeiCodeService', DateTime.now(), RegistrationNumberTypeEnum.LEI_CODE)
        )
      )
      subscriber.complete()
    })
  })
}
