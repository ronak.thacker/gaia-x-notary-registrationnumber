import { DateTime } from 'luxon'

import { RegistrationNumberTypeEnum } from '../enum/registration-number-type.enum'
import { EoriValidationPayload } from '../model/validation/eori-validation-payload'
import { LeiCodeValidationPayload } from '../model/validation/lei-code-validation-payload'
import { OpenCorporatesValidationPayload } from '../model/validation/open-corporates-validation-payload'
import { ValidationEvidence } from '../model/validation/validation-evidence'
import { ValidationPayload } from '../model/validation/validation-payload'
import { VatIdValidationPayload } from '../model/validation/vat-id-validation-payload'

export class RegistrationNumberTestUtils {
  static readonly VAT_ID: string = 'FR87880851399'
  static readonly LEI_CODE: string = '529900W18LQJJN6SJ336'
  static readonly EORI: string = 'FR42279772000121'
  static readonly EUID: string = 'DE50670B34567760'
  static readonly TAX_ID: string = '0762747721'

  static generateEveryValidationPayloadType() {
    return [
      new VatIdValidationPayload(
        RegistrationNumberTypeEnum.VAT_ID,
        RegistrationNumberTestUtils.VAT_ID,
        true,
        'FR',
        new ValidationEvidence('https://validator.eu/checkVatService', DateTime.now(), RegistrationNumberTypeEnum.VAT_ID)
      ),
      new LeiCodeValidationPayload(
        RegistrationNumberTypeEnum.LEI_CODE,
        RegistrationNumberTestUtils.LEI_CODE,
        true,
        {
          'iso3166-1': 'FR',
          'iso3166-2': 'FR-NAQ'
        },
        new ValidationEvidence('https://validator.eu/checkLeiCodeService', DateTime.now(), RegistrationNumberTypeEnum.LEI_CODE)
      ),
      new EoriValidationPayload(
        RegistrationNumberTypeEnum.EORI,
        RegistrationNumberTestUtils.EORI,
        true,
        'FR',
        new ValidationEvidence('https://validator.eu/checkEoriService', DateTime.now(), RegistrationNumberTypeEnum.EORI)
      ),
      new ValidationPayload(
        RegistrationNumberTypeEnum.EUID,
        RegistrationNumberTestUtils.EUID,
        true,
        new ValidationEvidence('https://validator.eu/checkEuidService', DateTime.now(), RegistrationNumberTypeEnum.EUID)
      ),
      new OpenCorporatesValidationPayload(
        RegistrationNumberTypeEnum.TAX_ID,
        RegistrationNumberTestUtils.TAX_ID,
        true,
        'BE',
        new ValidationEvidence('https://opencorporates.com/companies/be/0762747721', DateTime.now(), RegistrationNumberTypeEnum.TAX_ID)
      )
    ]
  }
}
