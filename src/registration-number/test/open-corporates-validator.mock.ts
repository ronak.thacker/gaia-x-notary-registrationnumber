import { DateTime } from 'luxon'
import { Observable } from 'rxjs'

import { RegistrationNumberTypeEnum } from '../enum/registration-number-type.enum'
import { OpenCorporatesValidationPayload } from '../model/validation/open-corporates-validation-payload'
import { ValidationEvidence } from '../model/validation/validation-evidence'
import { RegistrationNumberTestUtils } from './registration-number-test.utils'

export const OpenCorporatesValidatorMock = {
  validate: jest.fn((registrationNumber: string): Observable<OpenCorporatesValidationPayload> => {
    return new Observable(subscriber => {
      const valid = registrationNumber === RegistrationNumberTestUtils.TAX_ID

      subscriber.next(
        new OpenCorporatesValidationPayload(
          RegistrationNumberTypeEnum.TAX_ID,
          registrationNumber,
          valid,
          'BE',
          new ValidationEvidence('https://opencorporates.com/companies/be/0762747721', DateTime.now(), RegistrationNumberTypeEnum.TAX_ID)
        )
      )
      subscriber.complete()
    })
  })
}
