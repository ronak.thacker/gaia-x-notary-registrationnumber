import { DateTime } from 'luxon'
import { Observable } from 'rxjs'

import { RegistrationNumberTypeEnum } from '../enum/registration-number-type.enum'
import { ValidationEvidence } from '../model/validation/validation-evidence'
import { VatIdValidationPayload } from '../model/validation/vat-id-validation-payload'
import { RegistrationNumberTestUtils } from './registration-number-test.utils'

export const VatIdValidatorMock = {
  validate: jest.fn((vatId: string): Observable<VatIdValidationPayload> => {
    return new Observable(subscriber => {
      const valid = vatId === RegistrationNumberTestUtils.VAT_ID

      subscriber.next(
        new VatIdValidationPayload(
          RegistrationNumberTypeEnum.VAT_ID,
          vatId,
          valid,
          valid ? 'FR' : null,
          new ValidationEvidence('https://validator.eu/checkVatService', DateTime.now(), RegistrationNumberTypeEnum.VAT_ID)
        )
      )
      subscriber.complete()
    })
  })
}
