import { DateTime } from 'luxon'
import { Observable } from 'rxjs'

import { RegistrationNumberTypeEnum } from '../enum/registration-number-type.enum'
import { EoriValidationPayload } from '../model/validation/eori-validation-payload'
import { ValidationEvidence } from '../model/validation/validation-evidence'
import { RegistrationNumberTestUtils } from './registration-number-test.utils'

export const EoriValidatorMock = {
  validate: jest.fn((eori: string): Observable<EoriValidationPayload> => {
    return new Observable(subscriber => {
      const valid = eori === RegistrationNumberTestUtils.EORI

      subscriber.next(
        new EoriValidationPayload(
          RegistrationNumberTypeEnum.EORI,
          eori,
          valid,
          valid ? 'FR' : null,
          new ValidationEvidence('https://validator.eu/checkEoriService', DateTime.now(), RegistrationNumberTypeEnum.EORI)
        )
      )
      subscriber.complete()
    })
  })
}
