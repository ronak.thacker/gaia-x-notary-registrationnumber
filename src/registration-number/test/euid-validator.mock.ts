import { DateTime } from 'luxon'
import { Observable } from 'rxjs'

import { RegistrationNumberTypeEnum } from '../enum/registration-number-type.enum'
import { ValidationEvidence } from '../model/validation/validation-evidence'
import { ValidationPayload } from '../model/validation/validation-payload'
import { RegistrationNumberTestUtils } from './registration-number-test.utils'

export const EuidValidatorMock = {
  validate: jest.fn((euid: string): Observable<ValidationPayload> => {
    return new Observable(subscriber => {
      const valid = euid === RegistrationNumberTestUtils.EUID

      subscriber.next(
        new ValidationPayload(
          RegistrationNumberTypeEnum.EUID,
          euid,
          valid,
          new ValidationEvidence('https://validator.eu/checkEuidService', DateTime.now(), RegistrationNumberTypeEnum.EUID)
        )
      )
      subscriber.complete()
    })
  })
}
