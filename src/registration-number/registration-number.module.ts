import { HttpModule } from '@nestjs/axios'
import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'

import { RegistrationNumberController } from './controller/registration-number.controller'
import { PrivateKeyService } from './service/private-key.service'
import { RegistrationNumberService } from './service/registration-number.service'
import { SigningService } from './service/signing.service'
import { EoriValidator } from './validator/eori.validator'
import { EuidValidator } from './validator/euid.validator'
import { LeiCodeValidator } from './validator/lei-code.validator'
import { OpenCorporatesValidator } from './validator/open-corporates.validator'
import { RegistrationNumberValidatorFactory } from './validator/registration-number-validator.factory'
import { VatIdValidator } from './validator/vat-id.validator'

/**
 * This module is used to check participants' legal registration numbers
 */
@Module({
  imports: [ConfigModule, HttpModule],
  controllers: [RegistrationNumberController],
  providers: [
    RegistrationNumberService,
    RegistrationNumberValidatorFactory,
    SigningService,
    EoriValidator,
    VatIdValidator,
    EuidValidator,
    LeiCodeValidator,
    OpenCorporatesValidator,
    PrivateKeyService
  ]
})
export class RegistrationNumberModule {}
