export enum RegistrationNumberTypeEnum {
  EORI,
  EUID,
  LEI_CODE,
  VAT_ID,
  TAX_ID
}
