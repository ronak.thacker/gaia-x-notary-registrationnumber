// TODO OpenAPI documentation
import { ApiProperty } from '@nestjs/swagger'

/**
 * Represents the body of a registration number validation request
 */
export class ValidationRequestDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  'gx:EORI'?: string

  @ApiProperty()
  'gx:EUID'?: string

  @ApiProperty()
  'gx:vatID'?: string

  @ApiProperty()
  'gx:leiCode'?: string

  @ApiProperty()
  'schema:taxID'?: string
}
