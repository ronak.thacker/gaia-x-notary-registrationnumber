import { ApiProperty } from '@nestjs/swagger'

export class CredentialSubjectDto {
  @ApiProperty()
  type: 'gx:legalRegistrationNumber' | string

  @ApiProperty()
  id: string

  // VAT ID
  @ApiProperty()
  'gx:vatID'?: string

  @ApiProperty()
  'gx:vatID-countryCode'?: string

  // LEI Code
  @ApiProperty()
  'gx:leiCode'?: string

  @ApiProperty()
  'gx:leiCode-countryCode'?: string

  @ApiProperty()
  'gx:leiCode-subdivisionCountryCode'?: string

  // EORI
  @ApiProperty()
  'gx:EORI'?: string

  @ApiProperty()
  'gx:EORI-country'?: string

  // EUID
  @ApiProperty()
  'gx:EUID'?: string

  // TAX ID
  @ApiProperty()
  'schema:taxID'?: string
}
