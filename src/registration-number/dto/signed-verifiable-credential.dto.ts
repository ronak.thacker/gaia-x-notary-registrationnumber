import { Proof } from '@gaia-x/json-web-signature-2020'
import { ApiProperty } from '@nestjs/swagger'

import { CredentialSubjectDto } from './credential-subject.dto'
import { EvidenceDto } from './evidence.dto'

export class SignedVerifiableCredentialDto {
  @ApiProperty()
  '@context': string[]

  @ApiProperty()
  type: string[]

  @ApiProperty()
  id: string

  @ApiProperty()
  issuer: string

  @ApiProperty()
  issuanceDate: string

  @ApiProperty()
  credentialSubject: CredentialSubjectDto

  @ApiProperty()
  evidence: EvidenceDto[]

  @ApiProperty()
  proof?: Proof
}
