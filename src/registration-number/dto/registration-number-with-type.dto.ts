import { RegistrationNumberTypeEnum } from '../enum/registration-number-type.enum'

export class RegistrationNumberWithTypeDto {
  constructor(
    public readonly value: string,
    public readonly type: RegistrationNumberTypeEnum
  ) {}
}
