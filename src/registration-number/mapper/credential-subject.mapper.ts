import { CredentialSubjectDto } from '../dto/credential-subject.dto'
import { RegistrationNumberTypeEnum } from '../enum/registration-number-type.enum'
import { EoriValidationPayload } from '../model/validation/eori-validation-payload'
import { LeiCodeValidationPayload } from '../model/validation/lei-code-validation-payload'
import { ValidationPayload } from '../model/validation/validation-payload'
import { VatIdValidationPayload } from '../model/validation/vat-id-validation-payload'

export class CredentialSubjectMapper {
  static map(validationPayloads: ValidationPayload[], participantId: string): CredentialSubjectDto {
    const credentialSubject: CredentialSubjectDto = {
      id: participantId,
      type: 'gx:legalRegistrationNumber'
    }

    validationPayloads.forEach(payload => {
      if (payload.type === RegistrationNumberTypeEnum.VAT_ID) {
        credentialSubject['gx:vatID'] = payload.value
        credentialSubject['gx:vatID-countryCode'] = (<VatIdValidationPayload>payload).countryCode
      }

      if (payload.type === RegistrationNumberTypeEnum.LEI_CODE) {
        credentialSubject['gx:leiCode'] = payload.value
        credentialSubject['gx:leiCode-countryCode'] = (<LeiCodeValidationPayload>payload).countryCodes['iso3166-1']
        credentialSubject['gx:leiCode-subdivisionCountryCode'] = (<LeiCodeValidationPayload>payload).countryCodes['iso3166-2']
      }

      if (payload.type === RegistrationNumberTypeEnum.EORI) {
        credentialSubject['gx:EORI'] = payload.value
        credentialSubject['gx:EORI-country'] = (<EoriValidationPayload>payload).country
      }

      if (payload.type === RegistrationNumberTypeEnum.EUID) {
        credentialSubject['gx:EUID'] = payload.value
      }

      if (payload.type === RegistrationNumberTypeEnum.TAX_ID) {
        credentialSubject['schema:taxID'] = payload.value
      }
    })

    return credentialSubject
  }
}
