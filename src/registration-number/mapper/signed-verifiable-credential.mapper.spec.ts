import { VerifiableCredential } from '@gaia-x/json-web-signature-2020'
import { DateTime } from 'luxon'
import * as uuid from 'uuid'

import { SignedVerifiableCredentialDto } from '../dto/signed-verifiable-credential.dto'
import { ValidationPayload } from '../model/validation/validation-payload'
import { RegistrationNumberTestUtils } from '../test/registration-number-test.utils'
import { SignedVerifiableCredentialMapper } from './signed-verifiable-credential.mapper'

jest.mock('uuid')

describe('SignedVerifiableCredentialMapper', () => {
  const mockedUuid = '09eb3d9f-eb6e-43b1-9d8b-fe03701d64b5'

  beforeEach(() => {
    ;(uuid.v4 as jest.Mock).mockImplementation(() => mockedUuid)
  })

  it.each(['did:web:gaia-x.eu#legal-registration-number', undefined, null])(
    'should map validation payloads to a single signed verifiable credential DTO',
    (vcId: string) => {
      const validationPayloads: ValidationPayload[] = RegistrationNumberTestUtils.generateEveryValidationPayloadType()
      const didWeb = 'did:web:gaia-x.eu'
      const participantId = 'did:web:gaia-x.eu#participant'

      const result: SignedVerifiableCredentialDto = SignedVerifiableCredentialMapper.mapFromPayload(validationPayloads, didWeb, vcId, participantId)

      expect(result['@context']).toEqual([
        'https://www.w3.org/2018/credentials/v1',
        'https://w3id.org/security/suites/jws-2020/v1',
        'https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#',
        'https://schema.org/'
      ])
      expect(result.type).toEqual(['VerifiableCredential'])
      expect(result.id).toEqual(vcId ?? `${didWeb}:${mockedUuid}`)
      expect(result.issuer).toEqual(didWeb)
      expect(DateTime.fromISO(result.issuanceDate).diffNow().milliseconds).toBeLessThan(5000)
      expect(result.credentialSubject).toEqual({
        id: 'did:web:gaia-x.eu#participant',
        type: 'gx:legalRegistrationNumber',
        'gx:vatID': RegistrationNumberTestUtils.VAT_ID,
        'gx:vatID-countryCode': 'FR',
        'gx:leiCode': RegistrationNumberTestUtils.LEI_CODE,
        'gx:leiCode-countryCode': 'FR',
        'gx:leiCode-subdivisionCountryCode': 'FR-NAQ',
        'gx:EORI': RegistrationNumberTestUtils.EORI,
        'gx:EORI-country': 'FR',
        'gx:EUID': RegistrationNumberTestUtils.EUID,
        'schema:taxID': RegistrationNumberTestUtils.TAX_ID
      })

      expect(result.evidence).toHaveLength(5)
      expect(result.evidence.map(e => [e['gx:evidenceOf'], e['gx:evidenceURL']])).toEqual(
        expect.arrayContaining([
          ['VAT_ID', 'https://validator.eu/checkVatService'],
          ['LEI_CODE', 'https://validator.eu/checkLeiCodeService'],
          ['EORI', 'https://validator.eu/checkEoriService'],
          ['EUID', 'https://validator.eu/checkEuidService'],
          ['TAX_ID', 'https://opencorporates.com/companies/be/0762747721']
        ])
      )

      result.evidence.map(e => e['gx:executionDate']).forEach(date => expect(DateTime.fromISO(date).diffNow().milliseconds).toBeLessThan(5000))
    }
  )

  it('should correctly map from VerifiableCredential to SignedVerifiableCredentialDto', () => {
    const verifiableCredential: VerifiableCredential = {
      '@context': ['https://www.w3.org/2018/credentials/v1'],
      type: ['VerifiableCredential'],
      id: 'https://example.com/credentials/3732',
      issuer: 'https://example.com/issuers/14',
      issuanceDate: '2022-02-16T10:00:00Z',
      credentialSubject: {
        id: 'csId',
        type: 'gx:legalRegistrationNumber'
      },
      evidence: [
        {
          'gx:evidenceURL': 'http://ec.europa.eu/taxation_customs/vies/services/checkVatService',
          'gx:executionDate': '2023-10-16T10:18:36.651Z',
          'gx:evidenceOf': 'gx:vatID'
        }
      ],
      proof: {
        type: 'JsonWebSignature2020',
        created: '2023-10-16T10:19:08.539Z',
        proofPurpose: 'assertionMethod',
        verificationMethod: 'did:web:gaia-x.eu#JWK2020',
        jws: 'eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0I'
      }
    }

    const issuanceDate = DateTime.now().toISO()

    const result: SignedVerifiableCredentialDto = SignedVerifiableCredentialMapper.mapFromVerifiableCredential(verifiableCredential)

    expect(result).toEqual({
      '@context': ['https://www.w3.org/2018/credentials/v1'],
      type: ['VerifiableCredential'],
      id: 'https://example.com/credentials/3732',
      issuer: 'https://example.com/issuers/14',
      issuanceDate: '2022-02-16T10:00:00Z',
      credentialSubject: {
        id: 'csId',
        type: 'gx:legalRegistrationNumber'
      },
      evidence: [
        {
          'gx:evidenceURL': 'http://ec.europa.eu/taxation_customs/vies/services/checkVatService',
          'gx:executionDate': '2023-10-16T10:18:36.651Z',
          'gx:evidenceOf': 'gx:vatID'
        }
      ],
      proof: {
        type: 'JsonWebSignature2020',
        created: '2023-10-16T10:19:08.539Z',
        proofPurpose: 'assertionMethod',
        verificationMethod: 'did:web:gaia-x.eu#JWK2020',
        jws: 'eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0I'
      }
    })
  })
})
