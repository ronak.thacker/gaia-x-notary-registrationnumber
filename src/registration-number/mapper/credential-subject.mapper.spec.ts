import { CredentialSubjectDto } from '../dto/credential-subject.dto'
import { ValidationPayload } from '../model/validation/validation-payload'
import { RegistrationNumberTestUtils } from '../test/registration-number-test.utils'
import { CredentialSubjectMapper } from './credential-subject.mapper'

describe('CredentialSubjectMapper', () => {
  it('should map multiple validation payloads to a single credential subject DTO', () => {
    const validationPayloads: ValidationPayload[] = RegistrationNumberTestUtils.generateEveryValidationPayloadType()

    const result: CredentialSubjectDto = CredentialSubjectMapper.map(validationPayloads, 'did:web:gaia-x.eu#participant')

    expect(result).toEqual({
      id: 'did:web:gaia-x.eu#participant',
      type: 'gx:legalRegistrationNumber',
      'gx:vatID': RegistrationNumberTestUtils.VAT_ID,
      'gx:vatID-countryCode': 'FR',
      'gx:leiCode': RegistrationNumberTestUtils.LEI_CODE,
      'gx:leiCode-countryCode': 'FR',
      'gx:leiCode-subdivisionCountryCode': 'FR-NAQ',
      'gx:EORI': RegistrationNumberTestUtils.EORI,
      'gx:EORI-country': 'FR',
      'gx:EUID': RegistrationNumberTestUtils.EUID,
      'schema:taxID': RegistrationNumberTestUtils.TAX_ID
    })
  })

  it('should return an empty credential subject when validation payloads are empty', () => {
    const validationPayloads: ValidationPayload[] = []

    const result: CredentialSubjectDto = CredentialSubjectMapper.map(validationPayloads, 'did:web:gaia-x.eu#participant')

    expect(result).toEqual({
      id: 'did:web:gaia-x.eu#participant',
      type: 'gx:legalRegistrationNumber'
    })
  })
})
