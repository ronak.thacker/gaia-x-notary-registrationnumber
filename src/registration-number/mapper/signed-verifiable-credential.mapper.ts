import { VerifiableCredential } from '@gaia-x/json-web-signature-2020'
import { DateTime } from 'luxon'
import { v4 as uuid } from 'uuid'

import { SignedVerifiableCredentialDto } from '../dto/signed-verifiable-credential.dto'
import { OpenCorporatesValidationPayload } from '../model/validation/open-corporates-validation-payload'
import { ValidationPayload } from '../model/validation/validation-payload'
import { CredentialSubjectMapper } from './credential-subject.mapper'
import { EvidenceMapper } from './evidence.mapper'

export class SignedVerifiableCredentialMapper {
  static mapFromPayload(validationPayloads: ValidationPayload[], didWeb: string, vcId: string, participantId: string): SignedVerifiableCredentialDto {
    const contexts = [
      'https://www.w3.org/2018/credentials/v1',
      'https://w3id.org/security/suites/jws-2020/v1',
      'https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#'
    ]
    if (validationPayloads.filter(validationPayload => validationPayload instanceof OpenCorporatesValidationPayload).length > 0) {
      contexts.push('https://schema.org/')
    }
    return {
      '@context': contexts,
      type: ['VerifiableCredential'],
      id: vcId || `${didWeb}:${uuid()}`,
      issuer: didWeb,
      issuanceDate: DateTime.now().toISO(),
      credentialSubject: CredentialSubjectMapper.map(validationPayloads, participantId),
      evidence: validationPayloads.map(payload => payload.evidence).map(evidence => EvidenceMapper.map(evidence))
    }
  }

  static mapFromVerifiableCredential(verifiableCredential: VerifiableCredential): SignedVerifiableCredentialDto {
    return {
      ...verifiableCredential,
      '@context': verifiableCredential?.['@context'],
      type: ['VerifiableCredential'],
      id: verifiableCredential?.['id'],
      issuer: verifiableCredential?.['issuer'],
      issuanceDate: verifiableCredential?.['issuanceDate'] || DateTime.now().toISO(),
      credentialSubject: verifiableCredential?.['credentialSubject'],
      evidence: verifiableCredential?.['evidence']
    }
  }
}
