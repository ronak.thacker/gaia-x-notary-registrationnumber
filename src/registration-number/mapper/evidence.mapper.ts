import { EvidenceDto } from '../dto/evidence.dto'
import { RegistrationNumberTypeEnum } from '../enum/registration-number-type.enum'
import { ValidationEvidence } from '../model/validation/validation-evidence'

export class EvidenceMapper {
  static map(validationEvidence: ValidationEvidence): EvidenceDto {
    return {
      'gx:evidenceOf': RegistrationNumberTypeEnum[validationEvidence.of],
      'gx:evidenceURL': validationEvidence.url,
      'gx:executionDate': validationEvidence.executionDate.toISO()
    }
  }
}
