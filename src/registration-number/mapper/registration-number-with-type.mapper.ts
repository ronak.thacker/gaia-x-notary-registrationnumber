import { RegistrationNumberWithTypeDto } from '../dto/registration-number-with-type.dto'
import { ValidationRequestDto } from '../dto/validation-request.dto'
import { RegistrationNumberTypeEnum } from '../enum/registration-number-type.enum'

export class RegistrationNumberWithTypeMapper {
  static map(validationRequest: ValidationRequestDto): RegistrationNumberWithTypeDto[] {
    const registrationNumbers = []

    if (validationRequest['gx:EORI']?.trim()) {
      registrationNumbers.push(new RegistrationNumberWithTypeDto(validationRequest['gx:EORI']?.trim(), RegistrationNumberTypeEnum.EORI))
    }

    if (validationRequest['gx:EUID']?.trim()) {
      registrationNumbers.push(new RegistrationNumberWithTypeDto(validationRequest['gx:EUID']?.trim(), RegistrationNumberTypeEnum.EUID))
    }

    if (validationRequest['gx:vatID']?.trim()) {
      registrationNumbers.push(new RegistrationNumberWithTypeDto(validationRequest['gx:vatID']?.trim(), RegistrationNumberTypeEnum.VAT_ID))
    }

    if (validationRequest['gx:leiCode']?.trim()) {
      registrationNumbers.push(new RegistrationNumberWithTypeDto(validationRequest['gx:leiCode']?.trim(), RegistrationNumberTypeEnum.LEI_CODE))
    }

    if (validationRequest['schema:taxID']?.trim()) {
      registrationNumbers.push(new RegistrationNumberWithTypeDto(validationRequest['schema:taxID']?.trim(), RegistrationNumberTypeEnum.TAX_ID))
    }

    return registrationNumbers
  }
}
