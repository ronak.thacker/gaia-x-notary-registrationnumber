import { DateTime } from 'luxon'

import { RegistrationNumberTypeEnum } from '../enum/registration-number-type.enum'
import { ValidationEvidence } from '../model/validation/validation-evidence'
import { EvidenceMapper } from './evidence.mapper'

describe('EvidenceMapper', () => {
  it('should map validation evidence to an evidence DTO', () => {
    const now: DateTime = DateTime.now()
    const validationEvidence: ValidationEvidence = new ValidationEvidence('https://validator.eu/', now, RegistrationNumberTypeEnum.VAT_ID)

    expect(EvidenceMapper.map(validationEvidence)).toEqual({
      'gx:evidenceOf': 'VAT_ID',
      'gx:evidenceURL': 'https://validator.eu/',
      'gx:executionDate': now.toISO()
    })
  })
})
