import { RegistrationNumberWithTypeDto } from '../dto/registration-number-with-type.dto'
import { ValidationRequestDto } from '../dto/validation-request.dto'
import { RegistrationNumberTypeEnum } from '../enum/registration-number-type.enum'
import { RegistrationNumberTestUtils } from '../test/registration-number-test.utils'
import { RegistrationNumberWithTypeMapper } from './registration-number-with-type.mapper'

describe('RegistrationNumberWithTypeMapper', () => {
  it('should produce a registration number with type array', () => {
    const dto: ValidationRequestDto = {
      id: 'did:web:gaia-x.eu#participant-id',
      'gx:EORI': RegistrationNumberTestUtils.EORI,
      'gx:EUID': RegistrationNumberTestUtils.EUID,
      'gx:vatID': RegistrationNumberTestUtils.VAT_ID,
      'gx:leiCode': RegistrationNumberTestUtils.LEI_CODE,
      'schema:taxID': RegistrationNumberTestUtils.TAX_ID
    }

    expect(RegistrationNumberWithTypeMapper.map(dto)).toEqual([
      new RegistrationNumberWithTypeDto(RegistrationNumberTestUtils.EORI, RegistrationNumberTypeEnum.EORI),
      new RegistrationNumberWithTypeDto(RegistrationNumberTestUtils.EUID, RegistrationNumberTypeEnum.EUID),
      new RegistrationNumberWithTypeDto(RegistrationNumberTestUtils.VAT_ID, RegistrationNumberTypeEnum.VAT_ID),
      new RegistrationNumberWithTypeDto(RegistrationNumberTestUtils.LEI_CODE, RegistrationNumberTypeEnum.LEI_CODE),
      new RegistrationNumberWithTypeDto(RegistrationNumberTestUtils.TAX_ID, RegistrationNumberTypeEnum.TAX_ID)
    ])
  })

  it('should produce an empty registration number with type array', () => {
    const dto: ValidationRequestDto = {
      id: 'did:web:gaia-x.eu#participant-id',
      'gx:EORI': null,
      'gx:EUID': undefined,
      'gx:vatID': '',
      'gx:leiCode': ' ',
      'schema:taxID': '  '
    }

    expect(RegistrationNumberWithTypeMapper.map(dto)).toEqual([])
  })
})
