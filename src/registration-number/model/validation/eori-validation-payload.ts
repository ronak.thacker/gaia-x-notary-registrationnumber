import { RegistrationNumberTypeEnum } from '../../enum/registration-number-type.enum'
import { ValidationEvidence } from './validation-evidence'
import { ValidationPayload } from './validation-payload'

export class EoriValidationPayload extends ValidationPayload {
  constructor(
    type: RegistrationNumberTypeEnum,
    value: string,
    valid: boolean,
    private readonly _country: string,
    evidence: ValidationEvidence
  ) {
    super(type, value, valid, evidence)
  }

  get country(): string {
    return this._country
  }
}
