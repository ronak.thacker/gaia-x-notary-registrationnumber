import { RegistrationNumberTypeEnum } from '../../enum/registration-number-type.enum'
import { ValidationEvidence } from './validation-evidence'
import { ValidationPayload } from './validation-payload'

interface CountryCodes {
  'iso3166-1': string
  'iso3166-2': string
}

export class LeiCodeValidationPayload extends ValidationPayload {
  constructor(
    type: RegistrationNumberTypeEnum,
    value: string,
    valid: boolean,
    private readonly _countryCodes: CountryCodes,
    evidence: ValidationEvidence
  ) {
    super(type, value, valid, evidence)
  }

  get countryCodes(): CountryCodes {
    return this._countryCodes
  }
}
