import { RegistrationNumberTypeEnum } from '../../enum/registration-number-type.enum'
import { ValidationEvidence } from './validation-evidence'
import { ValidationPayload } from './validation-payload'

export class OpenCorporatesValidationPayload extends ValidationPayload {
  constructor(
    type: RegistrationNumberTypeEnum,
    value: string,
    valid: boolean,
    private readonly _countryCode: string,
    evidence: ValidationEvidence
  ) {
    super(type, value, valid, evidence)
  }

  get countryCode(): string {
    return this._countryCode
  }
}
