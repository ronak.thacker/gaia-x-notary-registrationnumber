import { RegistrationNumberTypeEnum } from '../../enum/registration-number-type.enum'
import { ValidationEvidence } from './validation-evidence'
import { ValidationPayload } from './validation-payload'

export class VatIdValidationPayload extends ValidationPayload {
  constructor(
    type: RegistrationNumberTypeEnum,
    value: string,
    valid: boolean,
    public readonly countryCode: string,
    evidence: ValidationEvidence,
    public readonly invalidationReason?: string
  ) {
    super(type, value, valid, evidence, invalidationReason)
  }
}
