import { Body, Controller, Header, HttpCode, Post, Query } from '@nestjs/common'
import { ApiBadRequestResponse, ApiBody, ApiExtraModels, ApiOkResponse, ApiOperation, ApiQuery, ApiTags, getSchemaPath } from '@nestjs/swagger'

import { SignedVerifiableCredentialDto } from '../dto/signed-verifiable-credential.dto'
import { ValidationRequestDto } from '../dto/validation-request.dto'
import { RegistrationNumberService } from '../service/registration-number.service'

const apiBody = {
  type: ValidationRequestDto,
  examples: {
    'VAT ID': {
      value: {
        '@context': ['https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/participant'],
        type: 'gx:legalRegistrationNumber',
        id: 'did:web:gaia-x.eu:legalRegistrationNumber.json',
        'gx:vatID': 'FR79537407926'
      }
    },
    'LEI code': {
      value: {
        '@context': ['https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/participant'],
        type: 'gx:legalRegistrationNumber',
        id: 'did:web:gaia-x.eu:legalRegistrationNumber.json',
        'gx:leiCode': '9695007586GCAKPYJ703'
      }
    },
    EORI: {
      value: {
        '@context': ['https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/participant'],
        type: 'gx:legalRegistrationNumber',
        id: 'did:web:gaia-x.eu:legalRegistrationNumber.json',
        'gx:EORI': 'FR53740792600014'
      }
    },
    'TAX ID': {
      value: {
        '@context': ['https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/participant', 'https://schema.org/'],
        type: 'gx:legalRegistrationNumber',
        id: 'did:web:gaia-x.eu:legalRegistrationNumber.json',
        'schema:taxID': '0762747721'
      }
    },
    All: {
      value: {
        '@context': ['https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/participant', 'https://schema.org/'],
        type: 'gx:legalRegistrationNumber',
        id: 'did:web:gaia-x.eu:legalRegistrationNumber.json',
        'gx:vatID': 'FR79537407926',
        'gx:leiCode': '9695007586GCAKPYJ703',
        'gx:EORI': 'FR53740792600014',
        'schema:taxID': '0762747721'
      }
    }
  }
}

const badRequestResponse = {
  description: `Invalid legal registration number verifiable credential`,
  content: {
    'application/json': {
      examples: {
        'Payload missing': {
          value: {
            statusCode: 400,
            message: 'The request payload is missing'
          }
        },
        'Participant ID missing': {
          value: {
            statusCode: 400,
            message: 'The participant ID is missing in the input verifiable credential'
          }
        },
        'Empty registration numbers': {
          value: {
            statusCode: 400,
            message: 'No registration number could be found in the input verifiable credential'
          }
        },
        'Invalid registration numbers': {
          value: {
            statusCode: 400,
            message: 'The following registration numbers are invalid: EORI[FR4227977200012], VAT_ID[FR8788085139]'
          }
        }
      }
    }
  }
}

@ApiTags('registration-number')
@Controller()
export class RegistrationNumberController {
  constructor(private readonly registrationNumberService: RegistrationNumberService) {}

  @Post('registrationNumberVC')
  @ApiOperation({
    summary: `Check and sign the legal registration numbers`,
    description:
      'This endpoint checks the given legal registration numbers against different official APIs to then sign it. A verifiable credential representing the legal registration numbers is returned.'
  })
  @HttpCode(200)
  @Header('Content-Type', 'application/json')
  @ApiQuery({
    name: 'vcid',
    description: `Overrides the output signed verifiable credential's ID`,
    type: 'string',
    required: false,
    example: 'did:web:gaia-x.eu#my-legal-registration-number'
  })
  @ApiBody(apiBody)
  @ApiExtraModels(SignedVerifiableCredentialDto)
  @ApiOkResponse({
    description: `Certified legal registration number verifiable credential`,
    content: {
      'application/json': {
        schema: {
          $ref: getSchemaPath(SignedVerifiableCredentialDto)
        },
        example: {
          '@context': [
            'https://www.w3.org/2018/credentials/v1',
            'https://w3id.org/security/suites/jws-2020/v1',
            'https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#',
            'https://schema.org/'
          ],
          type: ['VerifiableCredential'],
          id: 'did:web:gaia-x.eu#my-legal-registration-number',
          issuer: 'did:web:localhost',
          issuanceDate: '2023-12-01T11:17:58.430+01:00',
          credentialSubject: {
            id: 'did:web:gaia-x.eu:legalRegistrationNumber.json',
            type: 'gx:legalRegistrationNumber',
            'gx:EORI': 'FR53740792600014',
            'gx:EORI-country': '',
            'gx:vatID': 'FR79537407926',
            'gx:vatID-countryCode': 'FR',
            'gx:leiCode': '9695007586GCAKPYJ703',
            'gx:leiCode-countryCode': 'FR',
            'gx:leiCode-subdivisionCountryCode': null,
            'schema:taxID': '0762747721'
          },
          evidence: [
            {
              'gx:evidenceOf': 'EORI',
              'gx:evidenceURL': 'https://ec.europa.eu/taxation_customs/dds2/eos/validation/services/validation',
              'gx:executionDate': '2023-12-01T11:17:57.791+01:00'
            },
            {
              'gx:evidenceOf': 'VAT_ID',
              'gx:evidenceURL': 'https://ec.europa.eu/taxation_customs/vies/services/checkVatService',
              'gx:executionDate': '2023-12-01T11:17:57.864+01:00'
            },
            {
              'gx:evidenceOf': 'LEI_CODE',
              'gx:evidenceURL': 'https://api.gleif.org/api/v1/lei-records/9695007586GCAKPYJ703',
              'gx:executionDate': '2023-12-01T11:17:58.430+01:00'
            },
            {
              'gx:evidenceOf': 'TAX_ID',
              'gx:evidenceURL': 'https://opencorporates.com/companies/be/0762747721',
              'gx:executionDate': '2023-12-01T11:17:58.430+01:00'
            }
          ],
          proof: {
            type: 'JsonWebSignature2020',
            created: '2023-12-01T11:17:59.462+01:00',
            proofPurpose: 'assertionMethod',
            verificationMethod: 'did:web:localhost#X509-JWK2020',
            jws: 'eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..bQEm3Dl19rYdhcqyw-Nsg1kDr0PSjYTMpkHwHMcD9jmQJrWw60rvnG8S22Gt9CHPMAGpZVYUd4CcyEwY-oYKTRCq66bb_dKIr6yFxBI8Tn6mBvqRwEqCQqgiRNp3k3vMKI4chKhERrbPPj29r9rR5acstjdUA6cbNsZuncNjFnVZosZTuAKHjxey-1W0mAlLSEk-_nL9pzYKE6GgBjLDKsGwvgycwAa69CNunCMfiLcL-1Mhw0Klzjsyi6bKrNejqiuCITtT-osLGGSzGs74rxrW3KKIOdnI0fJOItcsHAWe1GrePuYs5WOPH24c_j0NYg1cjanysuxsUw_19t9GMQ'
          }
        }
      }
    }
  })
  @ApiBadRequestResponse(badRequestResponse)
  checkRegistrationNumberVC(@Body() body: ValidationRequestDto, @Query('vcid') vcId: string): Promise<SignedVerifiableCredentialDto> {
    return this.registrationNumberService.checkRegistrationNumberVC(body, vcId)
  }
}
