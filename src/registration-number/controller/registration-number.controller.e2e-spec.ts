import { ConfigService } from '@nestjs/config'
import { NestApplication } from '@nestjs/core'
import { Test } from '@nestjs/testing'
import * as jsonld from 'jsonld'
import { DateTime } from 'luxon'
import * as request from 'supertest'

import { ConfigServiceMock } from '../../common/test/config-service.mock'
import { KeyBuilder } from '../../utils/key-builder'
import { PrivateKeyService } from '../service/private-key.service'
import { RegistrationNumberService } from '../service/registration-number.service'
import { SigningService } from '../service/signing.service'
import { EoriValidatorMock } from '../test/eori-validator.mock'
import { EuidValidatorMock } from '../test/euid-validator.mock'
import { LeiCodeValidatorMock } from '../test/lei-code-validator.mock'
import { OpenCorporatesValidatorMock } from '../test/open-corporates-validator.mock'
import { RegistrationNumberTestUtils } from '../test/registration-number-test.utils'
import { VatIdValidatorMock } from '../test/vat-id-validator.mock'
import { EoriValidator } from '../validator/eori.validator'
import { EuidValidator } from '../validator/euid.validator'
import { LeiCodeValidator } from '../validator/lei-code.validator'
import { OpenCorporatesValidator } from '../validator/open-corporates.validator'
import { RegistrationNumberValidatorFactory } from '../validator/registration-number-validator.factory'
import { VatIdValidator } from '../validator/vat-id.validator'
import { RegistrationNumberController } from './registration-number.controller'

jest.mock('jsonld')

describe('RegistrationNumberController', () => {
  let app: NestApplication

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [RegistrationNumberController],
      providers: [
        RegistrationNumberService,
        RegistrationNumberValidatorFactory,
        ConfigService,
        SigningService,
        EuidValidator,
        EoriValidator,
        VatIdValidator,
        LeiCodeValidator,
        OpenCorporatesValidator,
        PrivateKeyService
      ]
    })
      .overrideProvider(EuidValidator)
      .useValue(EuidValidatorMock)
      .overrideProvider(EoriValidator)
      .useValue(EoriValidatorMock)
      .overrideProvider(VatIdValidator)
      .useValue(VatIdValidatorMock)
      .overrideProvider(LeiCodeValidator)
      .useValue(LeiCodeValidatorMock)
      .overrideProvider(OpenCorporatesValidator)
      .useValue(OpenCorporatesValidatorMock)
      .overrideProvider(ConfigService)
      .useValue(
        new ConfigServiceMock({
          BASE_URL: 'http://127.0.0.1:3000/main',
          PRIVATE_KEY: KeyBuilder.convertToPKCS8(KeyBuilder.buildKeyPair().privateKey),
          PRIVATE_KEY_ALGORITHM: 'PS256',
          OFFLINE_CONTEXTS: 'false'
        })
      )
      .compile()

    app = moduleRef.createNestApplication()
    await app.init()
  })

  afterAll(async () => {
    await app.close()
  })

  describe('/registrationNumberVC', () => {
    it('should validate and sign a verifiable credential', async () => {
      ;(jsonld.canonize as jest.Mock).mockResolvedValue('normalizedVerifiableCredential')

      const response = await request(app.getHttpServer())
        .post('/registrationNumberVC')
        .send({
          id: 'did:web:gaia-x.eu#participant',
          'gx:EORI': RegistrationNumberTestUtils.EORI,
          'gx:EUID': RegistrationNumberTestUtils.EUID,
          'gx:vatID': RegistrationNumberTestUtils.VAT_ID,
          'gx:leiCode': RegistrationNumberTestUtils.LEI_CODE,
          'schema:taxID': RegistrationNumberTestUtils.TAX_ID
        })
        .expect(200)

      expect(response.body.id).toMatch(/did:web:127\.0\.0\.1:[a-z0-9-]+/)
      expect(response.body.issuer).toEqual('did:web:127.0.0.1:3000:main')
      expect(DateTime.fromISO(response.body.issuanceDate).diffNow().milliseconds).toBeLessThan(5000)
      // Credential subject assertions
      expect(response.body.credentialSubject.id).toEqual('did:web:gaia-x.eu#participant')
      expect(response.body.credentialSubject.type).toEqual('gx:legalRegistrationNumber')
      expect(response.body.credentialSubject['gx:EORI']).toEqual(RegistrationNumberTestUtils.EORI)
      expect(response.body.credentialSubject['gx:EORI-country']).toEqual('FR')
      expect(response.body.credentialSubject['gx:EUID']).toEqual(RegistrationNumberTestUtils.EUID)
      expect(response.body.credentialSubject['gx:vatID']).toEqual(RegistrationNumberTestUtils.VAT_ID)
      expect(response.body.credentialSubject['gx:vatID-countryCode']).toEqual('FR')
      expect(response.body.credentialSubject['gx:leiCode']).toEqual(RegistrationNumberTestUtils.LEI_CODE)
      expect(response.body.credentialSubject['gx:leiCode-countryCode']).toEqual('FR')
      expect(response.body.credentialSubject['gx:leiCode-subdivisionCountryCode']).toEqual('FR-NAQ')
      expect(response.body.credentialSubject['schema:taxID']).toEqual(RegistrationNumberTestUtils.TAX_ID)
      // Evidence assertions
      expect(response.body.evidence.map(e => [e['gx:evidenceOf'], e['gx:evidenceURL']])).toEqual(
        expect.arrayContaining([
          ['EORI', 'https://validator.eu/checkEoriService'],
          ['EUID', 'https://validator.eu/checkEuidService'],
          ['VAT_ID', 'https://validator.eu/checkVatService'],
          ['LEI_CODE', 'https://validator.eu/checkLeiCodeService'],
          ['TAX_ID', 'https://opencorporates.com/companies/be/0762747721']
        ])
      )
      // Proof assertions
      expect(response.body.proof.jws).toBeTruthy()
    })

    it('should return a bad request if the participant ID is missing', () => {
      return request(app.getHttpServer())
        .post('/registrationNumberVC')
        .send({
          'gx:EORI': RegistrationNumberTestUtils.EORI,
          'gx:EUID': RegistrationNumberTestUtils.EUID,
          'gx:vatID': RegistrationNumberTestUtils.VAT_ID,
          'gx:leiCode': RegistrationNumberTestUtils.LEI_CODE,
          'schema:taxID': RegistrationNumberTestUtils.TAX_ID
        })
        .expect(400, {
          statusCode: 400,
          message: 'The participant ID is missing in the input verifiable credential'
        })
    })

    it('should return a bad request if the registration numbers are missing', () => {
      return request(app.getHttpServer())
        .post('/registrationNumberVC')
        .send({
          id: 'did:web:gaia-x.eu#participant'
        })
        .expect(400, {
          statusCode: 400,
          message: 'No registration number could be found in the input verifiable credential'
        })
    })

    it('should return a bad request if some registration numbers are invalid', () => {
      return request(app.getHttpServer())
        .post('/registrationNumberVC')
        .send({
          id: 'did:web:gaia-x.eu#participant',
          'gx:EORI': RegistrationNumberTestUtils.EORI,
          'gx:EUID': 'EUID123456789',
          'gx:vatID': RegistrationNumberTestUtils.VAT_ID,
          'gx:leiCode': 'LEI987654321',
          'schema:taxID': RegistrationNumberTestUtils.TAX_ID
        })
        .expect(400, {
          statusCode: 400,
          message: 'The following registration numbers are invalid: EUID[EUID123456789], LEI_CODE[LEI987654321]'
        })
    })
  })
})
