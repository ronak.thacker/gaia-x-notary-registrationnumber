import { HttpException, HttpStatus } from '@nestjs/common'
import { Test } from '@nestjs/testing'

import { SignedVerifiableCredentialDto } from '../dto/signed-verifiable-credential.dto'
import { ValidationRequestDto } from '../dto/validation-request.dto'
import { SignedVerifiableCredentialMapper } from '../mapper/signed-verifiable-credential.mapper'
import { EoriValidationPayload } from '../model/validation/eori-validation-payload'
import { LeiCodeValidationPayload } from '../model/validation/lei-code-validation-payload'
import { ValidationPayload } from '../model/validation/validation-payload'
import { VatIdValidationPayload } from '../model/validation/vat-id-validation-payload'
import { EoriValidatorMock } from '../test/eori-validator.mock'
import { EuidValidatorMock } from '../test/euid-validator.mock'
import { LeiCodeValidatorMock } from '../test/lei-code-validator.mock'
import { OpenCorporatesValidatorMock } from '../test/open-corporates-validator.mock'
import { RegistrationNumberTestUtils } from '../test/registration-number-test.utils'
import { VatIdValidatorMock } from '../test/vat-id-validator.mock'
import { EoriValidator } from '../validator/eori.validator'
import { EuidValidator } from '../validator/euid.validator'
import { LeiCodeValidator } from '../validator/lei-code.validator'
import { OpenCorporatesValidator } from '../validator/open-corporates.validator'
import { RegistrationNumberValidatorFactory } from '../validator/registration-number-validator.factory'
import { VatIdValidator } from '../validator/vat-id.validator'
import { RegistrationNumberService } from './registration-number.service'
import { SigningService } from './signing.service'

import Mock = jest.Mock

const hostname = 'gaia-x.eu'
const mockSigningService = {
  sign: jest.fn((validationPayloads: ValidationPayload[], participantId: string, vcId?: string): Promise<SignedVerifiableCredentialDto> => {
    return Promise.resolve(SignedVerifiableCredentialMapper.mapFromPayload(validationPayloads, `did:web:${hostname}`, vcId, participantId))
  })
}

const participantId = 'did:web:gaia-x.eu#participant'
const legalRegistrationNumberId = 'did:web:gaia-x.eu#legal-registration-number'
const validationRequest: ValidationRequestDto = {
  id: participantId,
  'gx:EORI': RegistrationNumberTestUtils.EORI,
  'gx:EUID': RegistrationNumberTestUtils.EUID,
  'gx:vatID': RegistrationNumberTestUtils.VAT_ID,
  'gx:leiCode': RegistrationNumberTestUtils.LEI_CODE,
  'schema:taxID': RegistrationNumberTestUtils.TAX_ID
}

describe('RegistrationNumberService', () => {
  let registrationNumberService: RegistrationNumberService

  beforeEach(async () => {
    jest.clearAllMocks()

    const moduleRef = await Test.createTestingModule({
      providers: [
        RegistrationNumberService,
        RegistrationNumberValidatorFactory,
        SigningService,
        EoriValidator,
        EuidValidator,
        VatIdValidator,
        LeiCodeValidator,
        OpenCorporatesValidator
      ]
    })
      .overrideProvider(EoriValidator)
      .useValue(EoriValidatorMock)
      .overrideProvider(EuidValidator)
      .useValue(EuidValidatorMock)
      .overrideProvider(VatIdValidator)
      .useValue(VatIdValidatorMock)
      .overrideProvider(LeiCodeValidator)
      .useValue(LeiCodeValidatorMock)
      .overrideProvider(OpenCorporatesValidator)
      .useValue(OpenCorporatesValidatorMock)
      .overrideProvider(SigningService)
      .useValue(mockSigningService)
      .compile()

    registrationNumberService = moduleRef.get<RegistrationNumberService>(RegistrationNumberService)
  })

  it.each([undefined, null, legalRegistrationNumberId])('should check registration number and sign it', async vcId => {
    await registrationNumberService.checkRegistrationNumberVC(validationRequest, vcId)

    expect(EoriValidatorMock.validate).toHaveBeenCalledTimes(1)
    expect(EuidValidatorMock.validate).toHaveBeenCalledTimes(1)
    expect(VatIdValidatorMock.validate).toHaveBeenCalledTimes(1)
    expect(LeiCodeValidatorMock.validate).toHaveBeenCalledTimes(1)
    expect(OpenCorporatesValidatorMock.validate).toHaveBeenCalledTimes(1)

    expect(mockSigningService.sign).toHaveBeenCalledTimes(1)
    const signingServiceCall = (mockSigningService.sign as Mock).mock.calls[0]
    expect(signingServiceCall[0]).toHaveLength(5)
    expect(signingServiceCall[0].map(payload => payload.constructor.name)).toEqual(
      expect.arrayContaining([EoriValidationPayload.name, ValidationPayload.name, VatIdValidationPayload.name, LeiCodeValidationPayload.name])
    )
    expect(signingServiceCall[1]).toEqual(participantId)
    expect(signingServiceCall[2]).toEqual(vcId)
  })

  it('should throw a bad request exception when payload is missing', () => {
    expect(registrationNumberService.checkRegistrationNumberVC(null, legalRegistrationNumberId)).rejects.toThrow(
      new HttpException('The request payload is missing', HttpStatus.BAD_REQUEST)
    )

    assertNoValidationOccurs()
  })

  it.each([null, undefined, '', ' '])('should throw a bad request exception when participant ID is missing', participantId => {
    const validationRequestMissingParticipantId: ValidationRequestDto = {
      id: participantId,
      'gx:EORI': RegistrationNumberTestUtils.EORI,
      'gx:EUID': RegistrationNumberTestUtils.EUID,
      'gx:vatID': RegistrationNumberTestUtils.VAT_ID,
      'gx:leiCode': RegistrationNumberTestUtils.LEI_CODE,
      'schema:taxID': RegistrationNumberTestUtils.TAX_ID
    }

    expect(registrationNumberService.checkRegistrationNumberVC(validationRequestMissingParticipantId, legalRegistrationNumberId)).rejects.toThrow(
      new HttpException('The participant ID is missing in the input verifiable credential', HttpStatus.BAD_REQUEST)
    )

    assertNoValidationOccurs()
  })

  it('should throw a bad request exception when no legal registration number is given', () => {
    const emptyValidationRequest: ValidationRequestDto = {
      id: participantId,
      'gx:EORI': null,
      'gx:EUID': null,
      'gx:vatID': null,
      'gx:leiCode': null,
      'schema:taxID': null
    }

    expect(registrationNumberService.checkRegistrationNumberVC(emptyValidationRequest, legalRegistrationNumberId)).rejects.toThrow(
      new HttpException('No registration number could be found in the input verifiable credential', HttpStatus.BAD_REQUEST)
    )

    assertNoValidationOccurs()
  })

  it('should throw a bad request exception when registration numbers are invalid', () => {
    expect(
      registrationNumberService.checkRegistrationNumberVC(
        {
          id: participantId,
          'gx:EORI': 'FR123456789',
          'gx:EUID': RegistrationNumberTestUtils.EUID,
          'gx:vatID': 'FR987654321',
          'gx:leiCode': RegistrationNumberTestUtils.LEI_CODE,
          'schema:taxID': RegistrationNumberTestUtils.TAX_ID
        },
        null
      )
    ).rejects.toThrow(
      new HttpException('The following registration numbers are invalid: EORI[FR123456789], VAT_ID[FR987654321]', HttpStatus.BAD_REQUEST)
    )

    expect(mockSigningService.sign).toHaveBeenCalledTimes(0)
  })

  function assertNoValidationOccurs() {
    expect(EoriValidatorMock.validate).toHaveBeenCalledTimes(0)
    expect(EuidValidatorMock.validate).toHaveBeenCalledTimes(0)
    expect(VatIdValidatorMock.validate).toHaveBeenCalledTimes(0)
    expect(LeiCodeValidatorMock.validate).toHaveBeenCalledTimes(0)
    expect(OpenCorporatesValidatorMock.validate).toHaveBeenCalledTimes(0)
    expect(mockSigningService.sign).toHaveBeenCalledTimes(0)
  }
})
