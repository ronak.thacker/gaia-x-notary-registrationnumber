import { HttpException, HttpStatus, Injectable } from '@nestjs/common'
import { from, lastValueFrom, mergeMap, toArray } from 'rxjs'

import { RegistrationNumberWithTypeDto } from '../dto/registration-number-with-type.dto'
import { SignedVerifiableCredentialDto } from '../dto/signed-verifiable-credential.dto'
import { ValidationRequestDto } from '../dto/validation-request.dto'
import { RegistrationNumberTypeEnum } from '../enum/registration-number-type.enum'
import { RegistrationNumberWithTypeMapper } from '../mapper/registration-number-with-type.mapper'
import { ValidationPayload } from '../model/validation/validation-payload'
import { RegistrationNumberValidatorFactory } from '../validator/registration-number-validator.factory'
import { SigningService } from './signing.service'

@Injectable()
export class RegistrationNumberService {
  constructor(
    private readonly validatorFactory: RegistrationNumberValidatorFactory,
    private readonly signingService: SigningService
  ) {}

  async checkRegistrationNumberVC(validationRequest: ValidationRequestDto, vcId?: string): Promise<SignedVerifiableCredentialDto> {
    const validationResults = await this.checkRequest(validationRequest)

    return this.signingService.sign(validationResults, validationRequest.id, vcId)
  }

  private async checkRequest(validationRequest: ValidationRequestDto) {
    if (!validationRequest) {
      throw new HttpException('The request payload is missing', HttpStatus.BAD_REQUEST)
    }

    if (!validationRequest.id || !validationRequest.id.trim()) {
      throw new HttpException('The participant ID is missing in the input verifiable credential', HttpStatus.BAD_REQUEST)
    }

    const registrationNumbers: RegistrationNumberWithTypeDto[] = RegistrationNumberWithTypeMapper.map(validationRequest)

    if (!registrationNumbers.length) {
      throw new HttpException('No registration number could be found in the input verifiable credential', HttpStatus.BAD_REQUEST)
    }

    const validationResults: ValidationPayload[] = await lastValueFrom(
      from(registrationNumbers).pipe(
        mergeMap(rn => this.validatorFactory.validatorFor(rn.type).validate(rn.value)),
        toArray()
      )
    )

    const invalidMessage: string = validationResults
      .filter(payload => !payload.valid)
      .map(payload => `${RegistrationNumberTypeEnum[payload.type]}[${payload.value}]`)
      .join(', ')

    if (invalidMessage.length > 0) {
      throw new HttpException(`The following registration numbers are invalid: ${invalidMessage}`, HttpStatus.BAD_REQUEST)
    }

    return validationResults
  }
}
