import { JsonWebSignature2020Signer, VerifiableCredential } from '@gaia-x/json-web-signature-2020'
import { SignerOptions } from '@gaia-x/json-web-signature-2020/dist/src/options/signer.options'
import { Injectable, InternalServerErrorException, Logger, OnModuleInit } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { KeyLike } from 'jose'

import { DidUtils } from '../../utils/did.utils'
import { SignedVerifiableCredentialDto } from '../dto/signed-verifiable-credential.dto'
import { OfflineDocumentLoader } from '../jsonld/offline-document.loader'
import { SignedVerifiableCredentialMapper } from '../mapper/signed-verifiable-credential.mapper'
import { ValidationPayload } from '../model/validation/validation-payload'
import { PrivateKeyService } from './private-key.service'


@Injectable()
export class SigningService implements OnModuleInit {
  private readonly logger = new Logger(SigningService.name)
  private readonly x509VerificationMethod = 'X509-JWK2020'
  private readonly didWeb: string
  private signer: JsonWebSignature2020Signer

  async onModuleInit() {
    const isOfflineContext = this.configService.get<string>('OFFLINE_CONTEXTS') === 'true'
    const privateKey: KeyLike = await this.privateKeyService.getPrivateKey()
    const signerOptions: SignerOptions = {
      safe: true,
      privateKey,
      privateKeyAlg: this.privateKeyService.getPrivateKeyAlgorithm(),
      verificationMethod: `${this.didWeb}#${this.x509VerificationMethod}`
    }
    if (isOfflineContext) {
      signerOptions.documentLoader = OfflineDocumentLoader.create()
    }
    this.signer = new JsonWebSignature2020Signer(signerOptions)
  }

  constructor(
    private readonly configService: ConfigService,
    private privateKeyService: PrivateKeyService
  ) {
    // This will always be provided as it is checked in the AppController
    const baseUrl = this.configService.get<string>('BASE_URL').trim()
    this.didWeb = DidUtils.buildDidWebFromBaseUrl(baseUrl)
  }

  async sign(validationPayloads: ValidationPayload[], participantId: string, vcId?: string): Promise<SignedVerifiableCredentialDto> {
    const verifiableCredential: SignedVerifiableCredentialDto = SignedVerifiableCredentialMapper.mapFromPayload(
      validationPayloads,
      this.didWeb,
      vcId,
      participantId
    )
    try {
      const signedVC: VerifiableCredential = await this.signer.sign(verifiableCredential)
      return SignedVerifiableCredentialMapper.mapFromVerifiableCredential(signedVC)
    } catch (e) {
      this.logger.error('Error occurred during signature', e)
      throw new InternalServerErrorException(e.message || e)
    }
  }
}
