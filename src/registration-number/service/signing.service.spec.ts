import { ImATeapotException, InternalServerErrorException } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { Test } from '@nestjs/testing'
import { DateTime } from 'luxon'

import { ConfigServiceMock } from '../../common/test/config-service.mock'
import { SignedVerifiableCredentialDto } from '../dto/signed-verifiable-credential.dto'
import { SignedVerifiableCredentialMapper } from '../mapper/signed-verifiable-credential.mapper'
import { ValidationPayload } from '../model/validation/validation-payload'
import { RegistrationNumberTestUtils } from '../test/registration-number-test.utils'
import { PrivateKeyService } from './private-key.service'
import { SigningService } from './signing.service'

describe('SigningService', () => {
  let signingService: SigningService

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [ConfigModule],
      providers: [SigningService, PrivateKeyService]
    })
      .overrideProvider(ConfigService)
      .useValue(
        new ConfigServiceMock({
          BASE_URL: 'https://gaia-x.eu/main',
          PRIVATE_KEY: 'fakePrivateKey',
          PRIVATE_KEY_ALGORITHM: 'fakePrivateKeyAlg',
          OFFLINE_CONTEXTS: 'false'
        })
      )
      .compile()

    signingService = moduleRef.get<SigningService>(SigningService)
  })

  it('should create a signed verifiable credential from validation payloads', async () => {
    const validationPayloads: ValidationPayload[] = RegistrationNumberTestUtils.generateEveryValidationPayloadType()
    const vcId = 'did:web:gaia-x.eu#legal-registration-number'
    const participantId = 'did:web:gaia-x.eu#participant'
    const vc = SignedVerifiableCredentialMapper.mapFromPayload(validationPayloads, `did:web:gaia-x.eu:main`, vcId, participantId)
    ;(signingService as any).signer = {
      sign: jest.fn().mockResolvedValue({
        ...vc,
        proof: {}
      })
    }

    const result: SignedVerifiableCredentialDto = await signingService.sign(validationPayloads, participantId, vcId)

    expect(result['@context']).toEqual([
      'https://www.w3.org/2018/credentials/v1',
      'https://w3id.org/security/suites/jws-2020/v1',
      'https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#',
      'https://schema.org/'
    ])
    expect(result.type).toEqual(['VerifiableCredential'])
    expect(result.id).toEqual(vcId)
    expect(result.issuer).toEqual(`did:web:gaia-x.eu:main`)
    expect(DateTime.fromISO(result.issuanceDate).diffNow().milliseconds).toBeLessThan(5000)

    expect(result.credentialSubject.id).toEqual(participantId)
    expect(result.credentialSubject.type).toEqual('gx:legalRegistrationNumber')
    expect(result.credentialSubject['gx:vatID']).toEqual(RegistrationNumberTestUtils.VAT_ID)
    expect(result.credentialSubject['gx:vatID-countryCode']).toEqual('FR')
    expect(result.credentialSubject['gx:leiCode']).toEqual(RegistrationNumberTestUtils.LEI_CODE)
    expect(result.credentialSubject['gx:leiCode-countryCode']).toEqual('FR')
    expect(result.credentialSubject['gx:leiCode-subdivisionCountryCode']).toEqual('FR-NAQ')
    expect(result.credentialSubject['gx:EORI']).toEqual(RegistrationNumberTestUtils.EORI)
    expect(result.credentialSubject['gx:EORI-country']).toEqual('FR')
    expect(result.credentialSubject['gx:EUID']).toEqual(RegistrationNumberTestUtils.EUID)
    expect(result.credentialSubject['schema:taxID']).toEqual(RegistrationNumberTestUtils.TAX_ID)

    expect(result.evidence.map(e => [e['gx:evidenceOf'], e['gx:evidenceURL']])).toEqual(
      expect.arrayContaining([
        ['VAT_ID', 'https://validator.eu/checkVatService'],
        ['LEI_CODE', 'https://validator.eu/checkLeiCodeService'],
        ['EORI', 'https://validator.eu/checkEoriService'],
        ['EUID', 'https://validator.eu/checkEuidService'],
        ['TAX_ID', 'https://opencorporates.com/companies/be/0762747721']
      ])
    )
    result.evidence.map(e => DateTime.fromISO(e['gx:executionDate'])).forEach(date => expect(date.diffNow().milliseconds).toBeLessThan(5000))

    expect(result.proof).toBeTruthy()
  })

  it('should throw exception when signature fails', async () => {
    const validationPayloads: ValidationPayload[] = RegistrationNumberTestUtils.generateEveryValidationPayloadType()
    const vcId = 'did:web:gaia-x.eu#legal-registration-number'
    const participantId = 'did:web:gaia-x.eu#participant'
    ;(signingService as any).signer = {
      sign: jest.fn().mockRejectedValue(new ImATeapotException('Mocked exception'))
    }
    try {
      await signingService.sign(validationPayloads, participantId, vcId)
    } catch (e) {
      expect(e).toBeInstanceOf(InternalServerErrorException)
      expect(e.message).toEqual('Mocked exception')

      return
    }
    throw new Error()
  })
})
