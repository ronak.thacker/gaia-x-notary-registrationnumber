import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'

import { CertificateController } from './controller/certificate.controller'
import { DidController } from './controller/did.controller'
import { DidService } from './service/did.service'

/**
 * This module is used to identify the Gaia-X Notary by exposing a Did and an x509 certificate chain
 */
@Module({
  imports: [ConfigModule],
  controllers: [DidController, CertificateController],
  providers: [DidService]
})
export class IdentityModule {}
