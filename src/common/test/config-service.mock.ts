/**
 * A simple {@link ConfigService} mock class with a straightforward usage.
 * <p>
 *   For example :
 *   ```js
 *   const configServiceMock = new ConfigServiceMock({
 *     MY_PROPERTY: 'testValue',
 *     ANOTHER_PROPERTY: 'anotherValue',
 *   }
 *   ```
 * </p>
 */
export class ConfigServiceMock {
  private readonly properties: Map<string, string>

  constructor(properties: any) {
    this.properties = new Map(Object.entries(properties))
  }

  get(property: string): string {
    return this.properties.get(property)
  }
}
