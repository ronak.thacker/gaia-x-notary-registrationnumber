import { CacheModule } from '@nestjs/cache-manager'
import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'

import { AppController } from './app.controller'
import { IdentityModule } from './identity/identity.module'
import { RegistrationNumberModule } from './registration-number/registration-number.module'

@Module({
  imports: [
    ConfigModule.forRoot(),
    CacheModule.register({
      isGlobal: true
    }),
    IdentityModule,
    RegistrationNumberModule
  ],
  controllers: [AppController],
  providers: []
})
export class AppModule {}
